const symbolX = 'x';
const symbolO = 'o';
const allCells = document.getElementsByClassName('cell');
let count = 1;
const winResults = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [6, 4, 2]
];

function playGame () {
  const cells = [];
  for (let i = 0; i < allCells.length; i++) {
    allCells[i].addEventListener('click', generateSymbol, false);
    const cell = {
      id: i,
      symbol: document.getElementById(`${i}`).innerText
    };
    cells.push(cell);
  }
  console.log(cells);
}

function resetGame () {
  count = 1;
  document.querySelector('h1').innerHTML = 'Next round: X';
  for (let i = 0; i < allCells.length; i++) {
    allCells[i].innerHTML = '';
    allCells[i].addEventListener('click', generateSymbol, false);
  }
}

function generateSymbol () {
  if (count % 2 === 0) {
    generateSymbolO();
  } else {
    generateSymbolX();
  }
  ++count;
}

function generateSymbolX () {
  document.getElementById(event.target.id).innerText = symbolX;
  document.querySelector('h1').innerHTML = 'Next round: O';
}

function generateSymbolO () {
  document.getElementById(event.target.id).innerText = symbolO;
  document.querySelector('h1').innerHTML = 'Next round: X';
}
